package com.example.mindiii.retrofitexample.data.remote;

import com.example.mindiii.retrofitexample.data.model.UserInfo;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by hemant.
 * Date: 21/12/18
 */

public interface ApiInterface {

    // For POST request

    // @FormUrlEncoded    // annotation that used with POST type request
    // @POST("/demo/login.php") // specify the sub url for our base url
    // public void login(
    //         @Field("user_email") String user_email,
    //         @Field("user_pass") String user_pass, Callback<SignUpResponse> callback);

//user_email and user_pass are the post parameters and SignUpResponse is a POJO class which recieves the response of this API


// for GET request

    //@GET("/demo/countrylist.php") // specify the sub url for our base url
    //public void getVideoList(Callback<List<CountryResponse>> callback);

// CountryResponse is a POJO class which receives the response of this API

   /* @GET("movie/top_rated")
    Call<MoviesResponse> getTopRatedMovies(@Query("api_key") String apiKey);

    @GET("movie/{id}")
    Call<MoviesResponse> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);*/

    @GET("user/userLogout")
    Call<ResponseBody> doUserLogout(@Header("authToken") String authToken, @Header("Language") String Language);

    @FormUrlEncoded
    @POST("userLogin")
    Call<UserInfo> doUserLogin(@Field("email") String email, @Field("password") String password,
                               @Field("deviceToken") String deviceToken, @Field("deviceType") String deviceType);

    @Multipart
    @POST("user/updateUserProfile")
    Call<ResponseBody> doUpdateProfile(@Header("authToken") String authToken,
                                       @Header("Language") String Language,
                                       @PartMap() Map<String, RequestBody> partMap,
                                       @Part MultipartBody.Part file);
}
