package com.example.mindiii.retrofitexample.data;

import com.example.mindiii.retrofitexample.data.remote.ApiInterface;

/**
 * Created by hemant.
 * Date: 21/12/18
 */

public interface DataManager extends ApiInterface {
}
