package com.example.mindiii.retrofitexample;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.mindiii.retrofitexample.data.model.UserInfo;
import com.example.mindiii.retrofitexample.utils.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.mindiii.retrofitexample.utils.AppUtils.createPartFromString;
import static com.example.mindiii.retrofitexample.utils.AppUtils.prepareFilePart;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private String token = "";
    private ImageView imgUser;
    private Uri uri;
    private ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btnLogin).setOnClickListener(this);
        findViewById(R.id.btnLogout).setOnClickListener(this);
        findViewById(R.id.btnUpdate).setOnClickListener(this);
        progress = findViewById(R.id.progress);
        imgUser = findViewById(R.id.imgUser);
        imgUser.setImageResource(R.drawable.ic_launcher_background);
        imgUser.setOnClickListener(this);
    }

    public void doLogin() {
        setLoading(true);
        Application.getDataManager().doUserLogin("jitendra@gmail.com", "123456", "", "").enqueue(new Callback<UserInfo>() {
            @Override
            public void onResponse(@NonNull Call<UserInfo> call, @NonNull Response<UserInfo> response) {
                try {
                    setLoading(false);
                    if (response.isSuccessful()) {

                        UserInfo userInfo = response.body();

                        assert userInfo != null;
                        token = userInfo.getUserDetail().getAuthToken();
                        Toast.makeText(getApplicationContext(), userInfo.getMessage(), Toast.LENGTH_LONG).show();
                    } else {
                        String errorMessage = response.errorBody().string();
                        Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
                    }
                } catch (IOException ex) {
                    Log.e("Exception login ", ex.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserInfo> call, @NonNull Throwable t) {
                setLoading(false);
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setLoading(boolean isLoad) {
        if (progress != null) progress.setVisibility(isLoad ? View.VISIBLE : View.GONE);
    }

    public void doLogout() {
        setLoading(true);
        Application.getDataManager().doUserLogout(token, "english").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                JSONObject obj;
                setLoading(false);
                try {
                    if (response.code() == 200) {
                        obj = new JSONObject(response.body().string());
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    } else if (response.code() == 400) {
                        obj = new JSONObject(String.valueOf(response.errorBody()));
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException ignored) {
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                setLoading(false);
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                doLogin();
                break;

            case R.id.btnLogout:
                doLogout();
                break;

            case R.id.btnUpdate:
                if (uri != null) doUpdateProfile();
                break;

            case R.id.imgUser:
                if (AppUtils.RequestMultiplePermissionCamera(this)) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(photoPickerIntent, 1);
                }
                break;

        }
    }

    private void doUpdateProfile() {
        setLoading(true);
        // create part for file (photo, video, ...)
        MultipartBody.Part body = prepareFilePart(this, "profileImage", uri);

// create a map of data to pass along
        RequestBody name = createPartFromString("Jitendra");
        RequestBody email = createPartFromString("jitendra@gmail.com");
        RequestBody contactNumber = createPartFromString("9617263736");
        RequestBody location = createPartFromString("Eastern Ring Rd, Scheme No 140, Indore, Madhya Pradesh 452016, India");
        RequestBody longitude = createPartFromString("75.9056639");
        RequestBody latitude = createPartFromString("22.7102623");

        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("name", name);
        map.put("email", email);
        map.put("contactNumber", contactNumber);
        map.put("location", location);
        map.put("longitude", longitude);
        map.put("latitude", latitude);

        Application.getDataManager().doUpdateProfile(token, "english", map, body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                JSONObject obj;
                setLoading(false);
                try {
                    if (response.code() == 200) {
                        assert response.body() != null;
                        obj = new JSONObject(response.body().string());
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    } else if (response.code() == 400) {
                        obj = new JSONObject(response.errorBody().string());
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                setLoading(false);
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 1:
                    try {
                        uri = data.getData();
                        if (uri != null) {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                            imgUser.setImageBitmap(bitmap);
                        }
                    } catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }


}
