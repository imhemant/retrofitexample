package com.example.mindiii.retrofitexample.data;

import com.example.mindiii.retrofitexample.data.model.UserInfo;
import com.example.mindiii.retrofitexample.data.remote.ApiInterface;
import com.example.mindiii.retrofitexample.data.remote.AppApiHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by hemant.
 * Date: 21/12/18
 */

public final class AppDataManager implements DataManager {

    private static AppDataManager instance;
    public final Gson mGson;
    private final ApiInterface mApiHelper;

    private AppDataManager() {
        mApiHelper = AppApiHelper.getAppApiInstance();
        mGson = new GsonBuilder().create();
    }

    public synchronized static AppDataManager getInstance() {
        if (instance == null) {
            instance = new AppDataManager();
        }
        return instance;
    }


    @Override
    public Call<ResponseBody> doUserLogout(String authToken, String Language) {
        return mApiHelper.doUserLogout(authToken, Language);
    }

    @Override
    public Call<UserInfo> doUserLogin(String email, String password, String deviceToken, String deviceType) {
        return mApiHelper.doUserLogin(email, password, deviceToken, deviceType);
    }

    @Override
    public Call<ResponseBody> doUpdateProfile(String authToken, String Language, Map<String, RequestBody> partMap, MultipartBody.Part file) {
        return mApiHelper.doUpdateProfile(authToken, Language, partMap, file);
    }
}
