package com.example.mindiii.retrofitexample.data.model;

/**
 * Created by hemant.
 * Date: 21/12/18
 */

public final class UserInfo {

    /**
     * status : success
     * message : Login successfully done!
     * userDetail : {"userId":"2","name":"jitendra","email":"jitendra@gmail.com","password":"$2y$10$1NwmD85pneqbRgLTYdrSFe2/CX45UcM1hVCGT3zAXnYEUlCSpWARy","profileImage":"https://dev.agrinvestonline.com/./uploads/profile/thumb/LlXob4m0jDaHOzRx.jpg","contactNumber":"9617263736","location":"Eastern Ring Rd, Scheme No 140, Indore, Madhya Pradesh 452016, India","longitude":"75.9056639","latitude":"22.7102623","language":"english","subscriptionPlan":"free","deviceType":"0","deviceToken":"","socialId":"","socialType":"","authToken":"bbedc60cd1251a721e5e8b8b266df8c4709c7913","status":"1","crd":"2018-10-16 10:35:37","upd":"2018-12-21 05:27:37"}
     */

    private String status;
    private String message;
    private UserDetailBean userDetail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDetailBean getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetailBean userDetail) {
        this.userDetail = userDetail;
    }

    public static class UserDetailBean {
        /**
         * userId : 2
         * name : jitendra
         * email : jitendra@gmail.com
         * password : $2y$10$1NwmD85pneqbRgLTYdrSFe2/CX45UcM1hVCGT3zAXnYEUlCSpWARy
         * profileImage : https://dev.agrinvestonline.com/./uploads/profile/thumb/LlXob4m0jDaHOzRx.jpg
         * contactNumber : 9617263736
         * location : Eastern Ring Rd, Scheme No 140, Indore, Madhya Pradesh 452016, India
         * longitude : 75.9056639
         * latitude : 22.7102623
         * language : english
         * subscriptionPlan : free
         * deviceType : 0
         * deviceToken :
         * socialId :
         * socialType :
         * authToken : bbedc60cd1251a721e5e8b8b266df8c4709c7913
         * status : 1
         * crd : 2018-10-16 10:35:37
         * upd : 2018-12-21 05:27:37
         */

        private String userId;
        private String name;
        private String email;
        private String password;
        private String profileImage;
        private String contactNumber;
        private String location;
        private String longitude;
        private String latitude;
        private String language;
        private String subscriptionPlan;
        private String deviceType;
        private String deviceToken;
        private String socialId;
        private String socialType;
        private String authToken;
        private String status;
        private String crd;
        private String upd;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getProfileImage() {
            return profileImage;
        }

        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        public String getContactNumber() {
            return contactNumber;
        }

        public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getSubscriptionPlan() {
            return subscriptionPlan;
        }

        public void setSubscriptionPlan(String subscriptionPlan) {
            this.subscriptionPlan = subscriptionPlan;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getSocialId() {
            return socialId;
        }

        public void setSocialId(String socialId) {
            this.socialId = socialId;
        }

        public String getSocialType() {
            return socialType;
        }

        public void setSocialType(String socialType) {
            this.socialType = socialType;
        }

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCrd() {
            return crd;
        }

        public void setCrd(String crd) {
            this.crd = crd;
        }

        public String getUpd() {
            return upd;
        }

        public void setUpd(String upd) {
            this.upd = upd;
        }
    }
}
