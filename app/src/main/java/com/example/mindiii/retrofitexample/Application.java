package com.example.mindiii.retrofitexample;

import android.content.Context;

import com.example.mindiii.retrofitexample.data.AppDataManager;


/**
 * Created by hemant.
 * Date: 21/12/18
 * Time: 2:34 PM
 */

public class Application extends android.app.Application {

    private static AppDataManager appInstance;
    private static Application instance;

    public static synchronized Application getInstance() {
        if (instance != null) {
            return instance;
        }
        return new Application();
    }

    public static AppDataManager getDataManager() {
        return appInstance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        appInstance = AppDataManager.getInstance();
    }

}
