package com.example.mindiii.retrofitexample.data.remote;

import com.example.mindiii.retrofitexample.BuildConfig;
import com.example.mindiii.retrofitexample.data.model.UserInfo;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hemant.
 * Date: 21/12/18
 */

public final class AppApiHelper implements ApiInterface {

    private static ApiInterface apiInterface = null;
    private static AppApiHelper apiHelper;

    public synchronized static AppApiHelper getAppApiInstance() {
        if (apiHelper == null) {
            apiHelper = new AppApiHelper();
        }
        return apiHelper;
    }

    private static ApiInterface getApiInterface() {
        if (apiInterface == null) {
            apiInterface = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(ApiInterface.class);
        }
        return apiInterface;
    }

    @Override
    public Call<ResponseBody> doUserLogout(String authToken, String Language) {
        return getApiInterface().doUserLogout(authToken, Language);
    }

    @Override
    public Call<UserInfo> doUserLogin(String email, String password, String deviceToken, String deviceType) {
        return getApiInterface().doUserLogin(email, password, deviceToken, deviceType);
    }

    @Override
    public Call<ResponseBody> doUpdateProfile(String authToken, String Language, Map<String, RequestBody> partMap, MultipartBody.Part file) {
        return getApiInterface().doUpdateProfile(authToken, Language, partMap, file);
    }
}